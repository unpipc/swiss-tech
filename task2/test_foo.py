def with_bar(cls):
    cls.bar = lambda self: None
    return cls


@with_bar
class A(object):
    def __init__(self, value):
        self._value = value

    def foo(self):
        self.bar()

        return self._value


def test():
    assert hasattr(A, 'bar')
