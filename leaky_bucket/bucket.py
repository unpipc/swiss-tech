#!/usr/bin/env python2.7

from datetime import datetime, timedelta
from threading import RLock


class LeakyBucket(object):
    def __init__(self, bucket_size, bucket_rate,
                 now_func=datetime.now):
        """
        Leaky Bucket as a meter algorithm:
        https://en.wikipedia.org/wiki/Leaky_bucket#As_a_meter

        :param bucket_size: (int) Size of the bucket in abstract units.
        :param bucket_rate: (int) Virtual flow rate (units per second).
        """

        assert isinstance(bucket_size, int)
        assert bucket_size > 0
        assert isinstance(bucket_rate, int)
        assert bucket_rate >= 0

        self._size = bucket_size
        self._rate = bucket_rate
        self._value = 0
        self._leaked = 0
        self._now_func = now_func
        self._flowed_at = self._now_func()
        self._lock = RLock()

    def __str__(self):
        return 'bucket[{}, {}/s]'.format(self._value, self._rate)

    def check(self, value):
        with self._lock:
            self.flow()
            return self._value + value <= self._size

    def leak(self, value):
        with self._lock:
            if self.check(value):
                self._value += value
                return True

        return False

    def flow(self):
        if self._rate > 0:
            gap = (self._now_func() - self._flowed_at).total_seconds()
            max_leaked = int(gap * self._rate)
            self._flowed_at += timedelta(seconds=float(max_leaked) / self._rate)
            self._leaked += min(self._value, max_leaked)
            self._value = max(0, self._value - max_leaked)

    value = property(lambda self: self._value)
    size = property(lambda self: self._size)
    rate = property(lambda self: self._rate)
    leaked = property(lambda self: self._leaked)
