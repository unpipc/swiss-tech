from pytest import raises, mark
from time import sleep
from bucket import LeakyBucket


@mark.parametrize('args', [(1.1, 100), (-1, 100),
                           (1, -1), (0, 1)])
def test_usage(args):
    with raises(AssertionError):
        LeakyBucket(*args)


def test_check():
    b = LeakyBucket(100, 1)

    assert b.check(50)
    assert b.check(100)
    assert not b.check(101)


def test_leak_no_traffic():
    b = LeakyBucket(100, 0)

    assert b.leak(50)
    assert b.leak(50)
    assert b.value == 100
    sleep(0.1)
    assert not b.check(1)
    assert not b.leak(1)


def test_leak():
    b = LeakyBucket(100, 1000)

    assert b.value == 0
    sleep(0.1)

    for _ in range(3):
        assert b.leak(100)
        assert b.value == 100
        sleep(0.1)


def test_leak_fractional():
    b = LeakyBucket(100, 9)

    assert b.leak(1)
    assert b.value == 1

    sleep(0.1)
    assert b.leak(1)
    # Nothing should have flowed as 9 * 0.1 < 1
    assert b.value == 2

    sleep(0.1)
    assert b.leak(1)
    # 9 * 0.2 = 1.8. One unit must have flowed:
    assert b.value == 2

    sleep(0.1)
    assert b.leak(1)
    # 9 * 0.3 = 2.7. Another unit must have flowed:
    assert b.value == 2
