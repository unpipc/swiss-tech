#!/usr/bin/env python2.7

import logging
import random
from threading import Thread
from datetime import datetime
from time import sleep
from bucket import LeakyBucket

logging.basicConfig(level=logging.INFO)
info = logging.info
debug = logging.debug

bucket = LeakyBucket(2000, 10000)


class BucketTestingThread(Thread):
    def __init__(self, sandbox, *args, **kwargs):
        super(BucketTestingThread, self).__init__(*args, **kwargs)
        self.terminate = False
        self.sandbox = sandbox
        self.sent = 0
        self.error = None

    def run(self):
        try:
            self.do()
        except Exception as e:
            self.error = e

    def do(self):
        raise NotImplemented


class Writer(BucketTestingThread):
    def do(self):
        while not self.terminate:
            value = random.randint(0, 100)
            result = bucket.leak(value)
            assert bucket.value <= bucket.size, \
                'Number of units in the bucket is greater than bucket size'

            ms = int((datetime.now() - self.sandbox.started_at).total_seconds() * 1000)
            debug('{}ms {}: sending {:3d} units: {}'.format(ms,
                                                            self.name,
                                                            value,
                                                            'ok' if result else 'x'))
            if result:
                self.sent += value

            if random.choice((True, True, True, False)):
                # Pause does not reveal concurrent access
                # problems:
                # pause = round(random(), 2) * .01
                pause = 0

                sleep(pause)


class Sandbox(object):
    def __init__(self, size):
        self.started_at = None
        self.stopped_at = None
        self.threads = [Writer(self) for _ in range(size)]
        self.__sent = None

    def start(self):
        self.started_at = datetime.now()

        for t in self.threads:
            t.start()

    def join(self):
        for t in self.threads:
            t.join()

    def stop(self):
        for t in self.threads:
            t.terminate = True

        for t in self.threads:
            t.join()

        # Actualize bucket value on process stop.
        bucket.flow()
        self.stopped_at = bucket._flowed_at

    def _sent(self):
        if self.__sent is None:
            self.__sent = sum(t.sent for t in self.threads)

        return self.__sent

    sent = property(lambda self: self._sent())

    def errors(self):
        return [t.error for t in self.threads if t.error]


def test_bucket_thread_safety():
    box = Sandbox(10)
    box.start()
    sleep(3)
    box.stop()

    time_span = (box.stopped_at - box.started_at).total_seconds()
    calc_total_leaked = box.sent - bucket.value
    send_rate = box.sent / time_span
    leak_rate = bucket.leaked / time_span
    calc_leak_rate = calc_total_leaked / time_span

    info('Elapsed time: {:.2f}s'.format(time_span))
    info('Total sent: {}'.format(box.sent))
    info('Total leaked: {}'.format(bucket.leaked))
    info('Total leaked (calculated): {}'.format(calc_total_leaked))
    info('Units in bucket: {}'.format(bucket.value))
    info('Average send rate: {:.2f}/s'.format(send_rate))
    info('Average leak rate: {:.2f}/s'.format(leak_rate))
    info('Average leak rate (calculated): {:.2f}/s'.format(calc_leak_rate))

    for t in box.threads:
        info('{} sent {:3d}'.format(t.name, t.sent))

    assert calc_leak_rate <= bucket.rate, \
        'Average leak rate ({:.2f}/s) is higher than ' \
        'built-in bucket rate ({:.2f}/s)'.format(leak_rate, bucket.rate)

    assert leak_rate == calc_leak_rate, 'Leak rates differ.'

    errors = box.errors()
    for e in errors:
        logging.error(e)

    assert not errors, 'There are errors in the threads.'


if __name__ == '__main__':
    test_bucket_thread_safety()
    logging.info('Test passed.')
